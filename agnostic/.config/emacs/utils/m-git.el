;;; m-git.el -*- lexical-binding: t; -*-

(setup (:elpa magit)
  (:with-hook magit-process-mode-hook
    (:hook 'goto-address-mode))
  (setq magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1)
  (setq transient-default-level 5)
  (setq magit-diff-refine-hunk nil)
  (setq magit-save-repository-buffers nil)
  (setq magit-revision-insert-related-refs nil)
  (setq transient-display-buffer-action '(display-buffer-below-selected))
  (setq magit-bury-buffer-function #'magit-mode-quit-window))

(require 'magit)
(defun magit-status-dotfiles ()
  "Like `magit-status' but with for my dotfiles folder."
  (interactive)
  (require 'magit-status)
  (let ((default-directory m-org-dir))
    (call-interactively #'magit-status)))

(setup (:elpa dired-git-info)
  (setq dgi-auto-hide-details-p nil)
  (add-hook 'dired-after-readin-hook 'dired-git-info-auto-enable))
(setup (:elpa dired-git))

(provide 'm-git)
