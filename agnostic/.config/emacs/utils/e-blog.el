;;; e-blog.el -*- lexical-binding: t; -*-

(setq hugo-blog-url "https://mfxarc.github.io/blog/")
(setq hugo-root-path (expand-file-name "blog/" m-data-dir))
(setq hugo-public-path (expand-file-name "public/" hugo-root-path))
(setq hugo-post-path (expand-file-name "blog/content/posts" m-data-dir))

(defun hugo-new-post (title)
  "New hugo blog post."
  (interactive "sPost title: ")
  (let* ((filename (downcase (replace-regexp-in-string "[[:space:]]+" "_" title)))
	 (fileorg (concat filename ".org"))
	 (filepathorg (expand-file-name fileorg hugo-post-path))
	 (filepath (expand-file-name filename hugo-post-path))
	 (fileother (expand-file-name title hugo-post-path))
	 (date (format-time-string "%Y-%m-%d")))
    (if (or (file-exists-p filepath)
	    (file-exists-p fileother)
	    (file-exists-p filepathorg)
	    (file-exists-p (expand-file-name title hugo-post-path)))
	(find-file filepath)
      (progn (make-empty-file filepathorg)
	     (find-file filepathorg)
	     (insert "#+title: " title "\n#+date: " date "\n#+type: post\n#+tags:"))
      (message filename))))

(defun hugo-delete-post ()
  "Delete hugo blog post."
  (interactive)
  (let* ((file (completing-read "Delete blog post: " (directory-files hugo-post-path nil ".org" nil nil)))
	 (filepath (expand-file-name file hugo-post-path)))
    (when (yes-or-no-p (format "Really want to delete this post?: %s" file))
      (progn (delete-file filepath)
	     (message "Post delete: %s" file)))))

(defun hugo-update-blog ()
  "Update hugo blog with new posts and changes."
  (interactive)
  (let ((default-directory hugo-root-path))
     (async-start-process "hugo-update" "hugo" (lambda (result) (message "Public folder updated.")) "--cleanDestinationDir" "--minify")))

(defun hugo-commit-blog ()
  "Commit hugo blog changes."
  (interactive)
  (let ((default-directory hugo-public-path))
    (async-start-process "hugo-add" "git"
			 (lambda (result)
			   (async-start-process "hugo-commit" "git"
						(lambda (result) (magit-push-current "origin/main" nil))
						"commit" "-q" "-m" "updated blog")) "add" "./*")))

(defun hugo-push-blog ()
  "Push hugo blog changes."
  (interactive)
  (let ((default-directory hugo-public-path))
    (magit-push-current "origin/main" nil)))

(defun hugo-lazy-update ()
  "Update, commit and push changes to blog."
  (interactive)
  (progn (hugo-update-blog)
	 (hugo-commit-blog)))

(defun hugo-private-update ()
  "Commit hugo blog changes."
  (interactive)
  (let ((default-directory hugo-root-path))
    (async-start-process "hugo-add" "git"
			 (lambda (result)
			   (async-start-process "hugo-commit" "git"
						(lambda (result) (magit-push-current "origin/main" nil))
						"commit" "-q" "-m" "updated blog")) "add" "./*")))

(defun hugo-find-post ()
  "List blog posts."
  (interactive)
  (hugo-new-post
   (completing-read "Select blog post: " (directory-files hugo-post-path nil ".org" nil nil))))

(defun hugo-ripgrep ()
  "Ripgrep in blog posts directory."
  (interactive)
  (consult-ripgrep hugo-post-path))

(defun hugo-open-blog ()
  "Open blog in browser."
  (interactive)
  (browse-url hugo-blog-url)
  (message "Opening blog in browser..."))

(defun hugo-open-preview ()
  "Open blog preview in browser."
  (interactive)
  (let ((default-directory hugo-root-path))
    (progn (async-shell-command "hugo serve")
	   (browse-url "http://localhost:1313/blog/")
	   (message "Opening preview in browser..."))))

(defun hugo-magit ()
  "Open magit-status in blog directory."
  (interactive)
  (let ((default-directory hugo-public-path))
    (magit-status-here)))

(provide 'e-blog)
