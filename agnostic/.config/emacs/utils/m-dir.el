;;; m-dir.el -*- lexical-binding: t; -*-

(setup dired
  (:also-elpa dired-hide-dotfiles dired-ranger)
  (setq sudo-edit-local-method "su")
  (setq dired-recursive-deletes 'always)
  (setq dired-recursive-copies 'always)
  (setq dired-create-destination-dirs 'always)
  (setq dired-auto-revert-buffer t)
  (setq dired-dwim-target t)
  (setq dired-hide-details-hide-symlink-targets nil)
  (setq dired-omit-verbose nil)
  ;;
  (setq dired-use-ls-dired nil)
  (setq dired-listing-switches "-vlAh --group-directories-first")
  (setq dired-omit-files "\\.bbl$\\|\\.tex$\\|\\.xml$\\|\\.bcf$\\|\\.run$\\|\\.out$\\|\\.log$\\|\\.fdb_latexmk$\\|\\.xdv$\\|\\.fls$\\|\\.srt$")
  ;;
  (setq large-file-warning-threshold nil)
  (setq dired-clean-confirm-killing-deleted-buffers nil)
  (setq dired-no-confirm '(byte-compile load chgrp chmod chown copy move hardlink symlink shell touch))
  (setq image-dired-thumb-size 150)
  (setq auto-revert-verbose 'nil)
  ;;
  (add-hook 'dired-mode-hook 'dired-omit-mode)
  (add-hook 'dired-mode-hook 'dired-async-mode)
  (add-hook 'dired-mode-hook 'auto-revert-mode)
  (add-hook 'dired-mode-hook 'dired-hide-details-mode))
(require 'dired)
(require 'dired-x)

(defun dired-next ()
  (interactive)
  (when (not (eq (line-number-at-pos) (line-number-at-pos (- (point-max) 1))))
    (next-line)))

(defun dired-prev ()
  (interactive)
  (when (not (eq (line-number-at-pos) 2))
    (previous-line)))

(defun dired-sort ()
  "Sort dired dir listing in different ways.
Prompt for a choice."
  (interactive)
  (let (sort-by arg)
    (setq sort-by (completing-read "Sort by:" '("name" "size" "date" "extension")))
    (pcase sort-by
      ("name" (setq arg "-vlAh --group-directories-first"))
      ("date" (setq arg "-vlAht --group-directories-first"))
      ("size" (setq arg "-vlAhS --group-directories-first"))
      ("extension" (setq arg "-vlahX --group-directories-first"))
      (otherwise (error "Dired-sort: unknown option %s" otherwise)))
    (dired-sort-other arg)))

(defun dired-up-directory ()
  "`dired-up-directory' in same buffer."
  (interactive)
  (find-alternate-file ".."))

(defun dired-find-file ()
  "Replace current dired buffer with file buffer or open it externally if needed."
  (interactive)
  (let* ((path (dired-get-file-for-visit))
	 (file (file-name-nondirectory path))
	 (name (file-name-base file))
	 (ext (file-name-extension file)))
    (pcase ext
      ((or "opus" "ogg" "mp3" "mp4" "mov" "mpg" "mpeg"
	   "xlsx" "pdf" "epub" "djvu" "ps"
	   "gif" "png" "jpg" "jpeg" "webp" "svg")
       (message "Opening %s externally" file)
       (call-process-shell-command (format "xdg-open \"%s\" &" path)))
      ((or "bz" "bz2" "tbz" "tbz2" "gz" "tgz" "xz" "tar")
       (progn (message "Extracting %s..." file)
	      (call-process-shell-command (format "tar xfv \"%s\" &" path))))
      ((or "docx")
       (progn (message "Converting %s..." file)
	      (call-process-shell-command (format "pandoc \"%s\" -o \"%s\" &" path (concat name ".odt")))))
      ((or "zip" "7z" "rar")
       (progn (message "Extracting %s..." file)
	      (call-process-shell-command (format "unzip \"%s\" -d \"%s\" &" path (concat name "/")))))
      (_ (find-alternate-file path)))))

(defun dired-copy-path ()
  "Copy selected file/directory path."
  (interactive)
  (let ((path (dired-get-file-for-visit)))
    (kill-new path)
    (message "Copied path - %s" path)))

(defun zathura-handler (operation &rest args)
  "An open hook that will invoke zathura when opening document files."
  (let ((file (car args)))
    (kill-buffer nil)
    (call-process-shell-command (format "zathura \"%s\" &" file))
    (recentf-add-file file)
    (error "Opened %s in zathura" (file-name-base file))))
(put 'zathura-handler 'operations '(insert-file-contents))
(add-to-list 'file-name-handler-alist '("\\.pdf\\|\\.epub\\|\\.ps\\|\\.mobi\\'" . zathura-handler))

(defun mpv-handler (operation &rest args)
  "An open hook that will invoke mpv when opening video/audio files."
  (let ((file (car args)))
    (kill-buffer nil)
    (call-process-shell-command (format "mpv \"%s\" &" file))
    (recentf-add-file file)
    (error "Opened %s in mpv" (file-name-base file))))
(put 'mpv-handler 'operations '(insert-file-contents))
(add-to-list 'file-name-handler-alist '("\\.mp4\\|\\.mkv\\|\\.webm\\'" . mpv-handler))

(defun sudo-find-file (file-name)
  "Like find file, but opens the file as root."
  (interactive "FSudo find: ")
  (let ((tramp-file-name (format "/sudo:root@%s:%s" system-name (expand-file-name file-name))))
    (find-file tramp-file-name)
    (recentf-add-file tramp-file-name)))

(defun sudo-edit-file ()
  "Edit current file as root."
  (interactive)
  (let ((tramp-file-name (format "/sudo:root@%s:%s" system-name (expand-file-name buffer-file-name))))
    (find-file tramp-file-name)
    (recentf-add-file tramp-file-name)))

(defun fd-file ()
  "Find any file in directories."
  (interactive)
  (let ((default-directory "~/"))
    (recentf-push
     (find-file
      (completing-read
       "Find file: "
       (process-lines "fd"
		      "--threads=4"
		      "--search-path=/media/offline"
		      "--search-path=/media/personal"
		      "--color=never"
		      "--regex"))))))

(defun dired-fm ()
  "Open dired buffer in GUI file manager."
  (interactive)
  (call-process-shell-command "thunar &"))

(defun dired-term ()
  "Open dired buffer in terminal."
  (interactive)
  (call-process-shell-command "st &"))

(defun dired-share-kdeconnect ()
  "Share file with kdeconnect."
  (interactive)
  (dired-do-shell-command "kdeconnect-handler" nil (dired-get-marked-files)))

(defun dired-classifier ()
  "Organize folder based on filetypes."
  (interactive)
  (when (eq major-mode 'dired-mode)
    (async-start-process "dired-classifier" "classifier" (lambda (result) (message "Folder organized.")))))

(provide 'm-dir)
