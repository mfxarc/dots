;;; m-ui.el -*- lexical-binding: t; -*-

(defvar theme-dark 'poetica-dark)
(defvar theme-light 'poetica)

(if (or (>= (string-to-number (format-time-string "%H" (current-time))) 16)
	(<= (string-to-number (format-time-string "%H" (current-time))) 10))
    (load-theme theme-dark t)
  (load-theme theme-light t))

(defvar current-theme (nth 0 custom-enabled-themes))

(defadvice load-theme (before theme-dont-propagate activate)
  "Disable theme before loading new one."
  (mapc #'disable-theme custom-enabled-themes))

(defun next-theme (theme)
  (if (eq theme 'default)
      (disable-theme current-theme)
    (load-theme theme t))
  (setq current-theme theme))

(defun toggle-theme ()
  "Cycle defined dark/light themes."
  (interactive)
  (cond ((eq current-theme theme-dark) (next-theme theme-light))
	((eq current-theme theme-light) (next-theme theme-dark))))

(set-face-attribute 'default nil :font "Iosevka" :height 170 :weight 'regular)
(set-fontset-font t 'hangul '("Sarasa Mono K" . "unicode-bmp"))
(set-fontset-font t 'symbol '("Symbola" . "unicode-bmp"))

(setup (:elpa solaire-mode)
  (solaire-global-mode))
(setq window-divider-default-places t)
(setq window-divider-default-right-width 1)
(setq window-divider-default-bottom-width 1)
(window-divider-mode)

(setup (:elpa mini-modeline)
  (setq mode-line '((:eval (cond (buffer-read-only (propertize "NO@" 'face '(:inherit font-lock-warning-face)))
				 ((buffer-modified-p) (propertize "!" 'face '(:inherit font-lock-warning-face)))))
		    (:eval (propertize " L%l:C%c " 'face '(:inherit font-lock-doc-face :height 160)))
		    (:eval (propertize (symbol-name major-mode) 'face '(:inherit font-lock-keyword-face :height 160)))))
  (setq mini-modeline-enhance-visual nil)
  (setq mini-modeline-r-format mode-line)
  (setq mini-modeline-display-gui-line nil)
  (mini-modeline-mode))

(setup (:elpa vertico)
  (:also-elpa marginalia orderless)
  (setq vertico-resize 'fixed)
  (setq vertico-cycle t)
  (setq vertico-count 7)
  (setq completion-styles '(orderless))
  (setq completion-category-defaults nil)
  (setq completion-category-overrides '((file (styles partial-completion))))
  (setq marginalia-annotator '(built-in))
  (setq history-length 35)
  (vertico-mode)
  (marginalia-mode)
  (savehist-mode 1))

(setup (:elpa consult)
  (setq consult-preview-key nil)
  (setq consult-buffer-sources '(consult--source-buffer consult--source-recent-file)))

(defun minibuffer-backward-kill (arg)
  "When minibuffer is completing a file name delete up to parent folder, otherwise delete normally"
  (interactive "p")
  (if minibuffer-completing-file-name
      (when (string-match-p "/." (minibuffer-contents))
	(zap-up-to-char (- arg) ?/))
    (unless (or (get-text-property (point) 'read-only)
		(eq (point) (point-min))
		(get-text-property (1- (point)) 'read-only))
      (delete-char -1))))

(setup (:elpa corfu)
  (setq corfu-cycle t)
  (setq corfu-auto t)
  (setq corfu-auto-prefix 0)
  (setq corfu-auto-delay 0.0)
  (setq corfu-commit-predicate nil)
  (setq corfu-quit-at-boundary t)
  (setq corfu-quit-no-match t)
  (setq corfu-echo-documentation nil)
  (setq completion-cycle-threshold 3)
  (corfu-global-mode))

(setup (:elpa cape)
  (add-to-list 'completion-at-point-functions 'cape-symbol))

(setup (:elpa neotree)
  (require 'neotree)
  (:option neo-theme 'nil
	   neo-hide-cursor t
	   neo-click-changes-root t
	   neo-cwd-line-style nil
	   neo-smart-open t
	   neo-show-slash-for-folder nil
	   neo-auto-indent-point t
	   neo-mode-line-type 'none
	   neo-window-width 18))

(defun neotree-back ()
  (interactive)
  (progn (goto-line 2)
	 (end-of-line)
	 (neotree-change-root)
	 (goto-line 3)))

(defun neotree-open ()
  (interactive)
  (progn (neotree-enter)
	 (goto-line 3)))

(defun neotree-next ()
  (interactive)
  (when (not (eq (line-number-at-pos) (line-number-at-pos (- (point-max) 1))))
    (neotree-next-line)))

(defun neotree-prev ()
  (interactive)
  (when (not (eq (line-number-at-pos) 3))
    (neotree-previous-line)))

(defun neotree-cycle (&optional arg)
  (interactive)
  (let ((neo-click-changes-root nil))
    (neo-buffer--execute arg nil 'neo-open-dir)))

(defun neotree-back ()
  (interactive)
  (progn (goto-line 2)
	 (end-of-line)
	 (neotree-change-root)
	 (goto-line 3)))

(defun neotree-open ()
  (interactive)
  (progn (neotree-enter)
	 (goto-line 3)))

(defun neotree-next ()
  (interactive)
  (when (not (eq (line-number-at-pos) (line-number-at-pos (- (point-max) 1))))
    (neotree-next-line)))

(defun neotree-prev ()
  (interactive)
  (when (not (eq (line-number-at-pos) 3))
    (neotree-previous-line)))

(defun neotree-cycle (&optional arg)
  (interactive)
  (let ((neo-click-changes-root nil))
    (neo-buffer--execute arg nil 'neo-open-dir)))

(defun neotree-toggle ()
  "Toggle show the NeoTree window."
  (interactive)
  (if (neo-global--window-exists-p)
      (neotree-hide)
    (progn (neotree-show)
	   (goto-line 3))))

(setup (:elpa which-key)
  (setq which-key-idle-delay 0.0)
  (setq which-key-add-column-padding 0)
  (setq which-key-sort-order 'which-key-key-order-alpha)
  (setq which-key-separator " - ")
  (setq which-key-side-window-slot 0)
  (setq which-key-min-display-lines 1)
  (which-key-mode)
  (which-key-setup-side-window-bottom))

(setup (:elpa hl-todo)
  (:hook-into prog-mode text-mode)
  (setq hl-todo-keyword-faces
	'(("WORK" . "#C3E88D")
	  ("FIXME" . "#BF616A")
	  ("TODO" . "#ECBE7B")
	  ("DONE" . "#D0CFD0")
	  ("CANCELLED" . "#BF616A")
	  ("NOTE" . "#82AAFF"))))

(setup (:elpa hl-line)
  (:hook-into prog-mode dired-mode youtube-search-mode))

(setup (:elpa rainbow-mode)
  (:also-elpa kurecolor)
  (:hook-into prog-mode text-mode fundamental-mode conf-mode))

(setup (:elpa centaur-tabs)
  (:option centaur-tabs-set-bar 'over
	   centaur-tabs-set-close-button nil
	   centaur-tabs-set-modified-marker t
	   centaur-tabs-modified-marker "!"
	   centaur-tabs-enable-ido-completion nil)
  (centaur-tabs-mode))
(dolist (buffers '("*Messa" "*Org" "*Back" "*Fly" "*ema" "*cal" "*Cal" "*WoMa"))
  (add-to-list 'centaur-tabs-excluded-prefixes buffers))

(defun centaur-tabs-buffer-groups ()
  (list (cond ((or (unless (derived-mode-p 'vterm-mode)
		     (string-equal "*" (substring (buffer-name) 0 1)))) "Emacs")
	      ((string-prefix-p "magit" (symbol-name major-mode)) "Magit")
	      ((derived-mode-p 'vterm-mode) "Terminal")
	      ((derived-mode-p 'emacs-lisp-mode) "Elisp")
	      ((derived-mode-p 'dired-mode) "Dired")
	      ((string-prefix-p "org-" (symbol-name major-mode)) "OrgMode")
	      ((memq major-mode '(css-mode html-mode)) "Web")
	      (t (centaur-tabs-get-group-name (current-buffer))))))

(setup (:elpa olivetti)
  (:hook-into org-mode-hook latex-mode-hook)
  (require 'olivetti))

(defun self-screenshot (&optional type)
  "Save a screenshot of type TYPE of the current Emacs frame.
    As shown by the function `', type can weild the value `svg',
    `png', `pdf'.

    This function will output in /tmp a file beginning with \"Emacs\"
    and ending with the extension of the requested TYPE."
  (interactive)
  (let* ((type (if type type
		 (intern (completing-read "Screenshot Type: " '(png pdf postscript)))))
	 (extension (pcase type
		      ('png        ".png")
		      ('pdf        ".pdf")
		      ('postscript ".ps")
		      (otherwise (error "Cannot export screenshot of type %s" otherwise))))
	 (filename (make-temp-file "Emacs-" nil extension))
	 (finalfile (expand-file-name (file-name-nondirectory filename) m-image-dir))
	 (data     (x-export-frames nil type)))
    (with-temp-file filename
      (insert data))
    (when (not (file-exists-p m-image-dir))
      (make-directory m-image-dir))
    (copy-file filename finalfile)
    (kill-new finalfile)
    (delete-file filename)
    (find-file finalfile)
    (message finalfile)))

(defun self-screenshot-png ()
  "Save a PNG screenshot of Emacs.
  See `self-screenshot'."
  (interactive)
  (self-screenshot 'png))

(defun make-window-half ()
  (interactive)
  (other-window 1)
  (enlarge-window (/ (window-height (next-window)) 2))
  (other-window 1))
(defun make-horizontal-half ()
  (interactive)
  (other-window 1)
  (enlarge-window-horizontally (/ (window-height (next-window)) 2))
  (other-window 1))
(defun kill-window ()
  (interactive)
  (if (and (>= (count-windows) 2)
	   (not (neo-global--window-exists-p)))
      (progn (kill-buffer (current-buffer))
	     (delete-window))
    (kill-buffer (current-buffer))))
(defun switch-to-previous-buffer ()
  (interactive)
  (switch-to-buffer (other-buffer (current-buffer) 1)))
(defun toggle-single-window ()
  (interactive)
  (if (= (count-windows) 1)
      (when single-window--last-configuration
	(set-window-configuration single-window--last-configuration))
    (setq single-window--last-configuration (current-window-configuration))
    (delete-other-windows)))
(defun toggle-window-split ()
  "switch between vertical and horizontal split only works for 2 windows"
  (interactive)
  (if (= (count-windows) 2)
      (let* ((this-win-buffer (window-buffer))
	     (next-win-buffer (window-buffer (next-window)))
	     (this-win-edges (window-edges (selected-window)))
	     (next-win-edges (window-edges (next-window)))
	     (this-win-2nd (not (and (<= (car this-win-edges)
					 (car next-win-edges))
				     (<= (cadr this-win-edges)
					 (cadr next-win-edges)))))
	     (splitter
	      (if (= (car this-win-edges)
		     (car (window-edges (next-window))))
		  'split-window-horizontally
		'split-window-vertically)))
	(delete-other-windows)
	(let ((first-win (selected-window)))
	  (funcall splitter)
	  (if this-win-2nd (other-window 1))
	  (set-window-buffer (selected-window) this-win-buffer)
	  (set-window-buffer (next-window) next-win-buffer)
	  (select-window first-win)
	  (if this-win-2nd (other-window 1))))))

(setup (:elpa scratch))

(provide 'm-ui)
