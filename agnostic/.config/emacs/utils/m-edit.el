;;; m-edit.el -*- lexical-binding: t; -*-

(setup (:elpa avy)
  (:option avy-keys '(?w ?e ?a ?s ?d ?h ?j ?k ?l)))

(setup (:elpa goto-last-change))

(setup (:elpa crux))

(defun end-line ()
  "Go to end of line."
  (interactive)
  (if (use-region-p)
      (progn (end-of-line) (backward-char) (forward-char))
    (end-of-line)))

(defun move-line-up ()
  "Move up the current line."
  (interactive)
  (transpose-lines 1)
  (forward-line -2)
  (indent-according-to-mode))

(defun move-line-down ()
  "Move down the current line."
  (forward-line 1)
  (transpose-lines 1)
  (forward-line -1)
  (interactive)
  (indent-according-to-mode))

(setup (:elpa undo-fu)
  (:option evil-undo-system 'undo-fu))
(setup (:elpa undo-fu-session)
  (global-undo-fu-session-mode))

(setup (:elpa rotate-text)
  (:option rotate-text-words '(("true" "false")
			       ("enable" "disable")
			       ("t" "nil")
			       ("width" "height")
			       ("left" "right" "top" "bottom")
			       )))

(setup (:elpa evil-nerd-commenter))

(setup (:elpa string-inflection))

(defun mark-current-word (&optional arg allow-extend)
  "Put point at beginning of current word, set mark at end."
  (interactive "p\np")
  (setq arg (if arg arg 1))
  (if (and allow-extend
	   (or (and (eq last-command this-command) (mark t))
	       (region-active-p)))
      (set-mark
       (save-excursion
	 (when (< (mark) (point))
	   (setq arg (- arg)))
	 (goto-char (mark))
	 (forward-word arg)
	 (point)))
    (let ((wbounds (bounds-of-thing-at-point 'word)))
      (unless (consp wbounds)
	(error "No word at point"))
      (if (>= arg 0)
	  (goto-char (car wbounds))
	(goto-char (cdr wbounds)))
      (push-mark (save-excursion
		   (forward-word arg)
		   (point)))
      (activate-mark))))

(defun surround-word-or-region (char1 char2)
  (let* ((bds (bounds-of-thing-at-point 'word))
	 (beg (region-beginning))
	 (end (region-end))
	 (text (buffer-substring beg end)))
    (if (use-region-p)
	(when beg (delete-region beg end)
	      (progn (goto-char (region-beginning))
		     (insert char1)
		     (insert text)
		     (goto-char (region-end))
		     (insert char2)))
      (progn (goto-char (car bds))
	     (insert char1)
	     (goto-char (+ (cdr bds) 1))
	     (insert char2)))))

(defun insert-parentheses ()
  (interactive)
  (surround-word-or-region "\(" "\)"))
(defun insert-brackets ()
  (interactive)
  (surround-word-or-region "\{" "\}"))
(defun insert-quotes ()
  (interactive)
  (surround-word-or-region "\'" "\'"))
(defun insert-double-quotes ()
  (interactive)
  (surround-word-or-region "\"" "\""))
(defun insert-writer-quotes ()
  (interactive)
  (surround-word-or-region "\“" "\”"))

(setup (:elpa evil-surround)
  (global-evil-surround-mode))

(setup (:elpa yasnippet)
  (:also-elpa yasnippet-snippets)
  (yas-global-mode)
  (add-to-list 'yas-snippet-dirs (concat m-cache-dir "snippets/")))

(setup (:elpa lorem-ipsum))

(defun insert-hour()
  (interactive)
  (forward-char)
  (insert (format-time-string "%H:%M"))
  (evil-insert 1))
(defun insert-date()
  (interactive)
  (forward-char)
  (insert (format-time-string "%d/%m/%Y"))
  (evil-insert 1))
(defun prompt-date()
  (interactive)
  (let ((date (format-time-string "%d/%m/%Y - %H:%M" (org-read-date nil 'to-time nil "Choose date :  "))))
    (forward-char)
    (insert date)
    (evil-insert 1)))

(provide 'm-edit)
