;;; e-note.el -*- lexical-binding: t; -*-

(defvar notes-path nil
  "Default path to notes.")

(defun notes--list ()
  "List notes."
  (directory-files notes-path nil ".org" nil nil))

(defun note-create (title)
  "New note with TITLE as argument."
  (interactive "sNote name: ")
  (let* ((filename (downcase (replace-regexp-in-string "[[:space:]]+" "_" title)))
	 (fileorg (concat filename ".org"))
	 (filepathorg (expand-file-name fileorg notes-path))
	 (filepath (expand-file-name filename notes-path))
	 (fileother (expand-file-name title notes-path))
	 (date (format-time-string "%Y-%m-%d")))
    (if (or (file-exists-p fileother)
	    (file-exists-p filepath)
	    (file-exists-p filepathorg))
	(find-file (expand-file-name title notes-path))
      (progn (make-empty-file filepathorg)
	     (find-file filepathorg)
	     (insert "#+title: " title "\n#+date: " date)
	     (insert "\n\n")
	     (goto-line 4)
	     (write-file filepathorg)))))

(defun notes-find ()
  "Find notes."
  (interactive)
  (note-create (completing-read "Select note: " (notes--list))))

(provide 'e-note)
