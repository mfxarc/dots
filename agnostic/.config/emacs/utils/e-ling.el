;;; e-ling.el -*- lexical-binding: t; -*-

(setq default-input-method "ipa-praat")

(setq ispell-program-name "hunspell")
(setq ispell-dictionary "pt_BR")
(setq flyspell-issue-message-flag nil)
(setup (:elpa consult-flyspell))
(setup (:elpa flyspell-correct))
(setq consult-flyspell-select-function 'flyspell-auto-correct-word)
(add-hook 'org-mode 'flyspell-mode)
(add-hook 'text-mode 'flyspell-mode)

(let ((langs '("en_US" "pt_BR")))
  (setq lang-ring (make-ring (length langs)))
  (dolist (elem langs) (ring-insert lang-ring elem)))

(defun cycle-ispell-languages ()
  (interactive)
  (let ((lang (ring-ref lang-ring -1)))
    (ring-insert lang-ring lang)
    (ispell-change-dictionary lang)))

(define-derived-mode dict-mode fundamental-mode "dict"
  "major mode for editing my language code."
  (setq font-lock-defaults '(("-->\\([^<]+?\\)$" . 'font-lock-function-name-face)))
  (goto-char 0)
  (toggle-word-wrap)
  (read-only-mode))

(defun define-word-at-point ()
  "Define word at point and print results in buffer."
  (interactive)
  (let* ((word (current-word))
	 (buffer (get-buffer-create (concat word "-definition"))))
    (with-current-buffer buffer
      (progn (insert (shell-command-to-string (format "sdcv -0ne %s" word)))
	     (flush-lines "^ " (point-min) (point-max))
	     (flush-lines "^$" (point-min) (point-max))
	     (flush-lines "• " (point-min) (point-max))
	     (flush-lines "^Found" (point-min) (point-max))
	     (replace-regexp "	" "+ " nil (point-min) (point-max))
	     (delete-duplicate-lines (point-min) (point-max))
	     (dict-mode)))
    (display-buffer buffer '(display-buffer-at-bottom . nil))
    (switch-to-buffer-other-window buffer)))

(defun speak-at-point ()
  "Talk word or region at point"
  (interactive)
  (let* ((language (completing-read "Speak in: " '("en" "fr" "ko" "it" "pt")))
	 (word (prin1-to-string (if (use-region-p)
				    (buffer-substring-no-properties (region-beginning) (region-end))
				  (current-word)))))
    (async-start `(lambda () (shell-command-to-string (format "trans -b -p :%s %s" ,language ,word))) nil)))

(defun translate-at-point ()
  "Translate word or region at point"
  (interactive)
  (let* ((language (completing-read "Translate to: " '("en" "fr" "ko" "it" "pt" "es")))
	 (word (prin1-to-string (if (use-region-p)
				    (buffer-substring-no-properties (region-beginning) (region-end))
				  (current-word)))))
    (async-start `(lambda () (shell-command-to-string (format "trans -b :%s %s" ,language ,word)))
		 (lambda (result)
		   (pos-tip-show-no-propertize result 'default)
		   (kill-new result)
		   (message result)))))

(provide 'e-ling)
