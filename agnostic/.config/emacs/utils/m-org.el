;;; m-org.el -*- lexical-binding: t; -*-

(setup org
  (:with-hook org-mode-hook
    (:hook 'prettify-symbols-mode))
  (:option org-confirm-babel-evaluate nil
	   org-link-elisp-confirm-function nil
	   org-M-RET-may-split-line nil
	   org-insert-heading-respect-content t
	   org-return-follows-link t
	   org-mouse-1-follows-link nil
	   ;; Source block
	   org-src-preserve-indentation nil
	   org-src-tab-acts-natively nil
	   org-src-window-setup 'current-window
	   ;; Appearance
	   org-auto-align-tags t
	   org-blank-before-new-entry nil
	   org-fontify-quote-and-verse-blocks nil
	   org-fontify-whole-heading-line nil
	   org-fontify-whole-block-delimiter-line nil
	   org-edit-src-content-indentation 0
	   org-odt-fontify-srcblocks nil
	   org-hide-leading-stars nil
	   org-cycle-separator-lines 0
	   org-descriptive-links t
	   org-adapt-indentation t
	   org-cycle-separator-lines 0
	   org-imenu-depth 3
	   org-log-done 'time
	   org-startup-truncated t
	   org-startup-folded 'content
	   org-support-shift-select 'always
	   org-hide-emphasis-markers t
	   org-todo-window-setup 'current-window
	   org-log-into-drawer t
	   org-todo-keywords '((sequence "TODO(t)" "RECALL(r)" "|" "DONE(d)" "WAIT(w)" "CANCELLED(c)" ))
	   ;;; Images
	   image-use-external-converter t
	   image-converter 'imagemagick
	   preview-image-type 'svg
	   org-startup-with-inline-images t
	   org-image-actual-width '(500)
	   ;; Agenda
	   org-agenda-sorting-strategy '(scheduled-up)
	   org-agenda-include-deadlines t
	   org-agenda-skip-deadline-if-done t
	   org-agenda-skip-scheduled-if-done t
	   org-agenda-skip-unavailable-files t
	   org-agenda-window-setup 'current-window
	   org-agenda-start-on-weekday nil
	   org-agenda-span 'day
	   org-agenda-inhibit-startup t))

(defface org-horizontal-rule
  '((default :inherit org-hide) (((background light)) :strike-through "gray70") (t :strike-through "gray30"))
  "Face used for horizontal ruler.")

(font-lock-add-keywords
 'org-mode
 '(
   ;; Replace lists "-" and "+" with unicode characters
   ("^ *\\([-]\\) " (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))
   ("^ *\\([+]\\) " (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "◦"))))
   ;; Turn page break in pretty horizontal lines
   ("^-\\{5,\\}$" 0 '(face org-horizontal-rule display (space :width text)))
   ;; Hide heading leading stars
   ("^\\*+ " (0 (prog1 nil (put-text-property (match-beginning 0)
					      (match-end 0) 'face
					      (list :foreground (face-attribute 'default :background))))))
   ))

(setq-default prettify-symbols-alist
	      '(("#+begin_quote" . "")
		("#+end_quote" . "")
		("#+begin_src" . "")
		("#+end_src" . "")
		("#+begin_myframe" . "")
		("#+end_myframe" . "")
		("#+title:" . "")
		("#+nota:" . "")
		("#+filetags:" . "")
		("#+tags:" . "")
		("#+id:" . "")
		("#+topic:" . "")
		("#+type:" . "")
		("#+author:" . "")
		("#+date:" . "")
		("#+time:" . "")
		("#+TBLFM:" . "")
		("#+setupfile:" . "")
		(":PROPERTIES:" . "")
		("[ ]" . "")
		("[X]" . "")))

(setup (:elpa org-appear)
  (:hook-into org-mode-hook))
(setup (:elpa valign)
  (:hook-into org-mode-hook)
  (:option valign-fancy-bar t))

(dolist (languages '(ob-awk ob-shell ob-emacs-lisp ob-lua ob-gnuplot ob-dot
			    ob-calc ob-lilypond org-tempo ox-latex))
  (require languages))
(setup (:elpa gnuplot))
(add-to-list 'org-babel-after-execute-hook 'org-redisplay-inline-images)
(add-to-list 'org-structure-template-alist '("hs" . "src haskell"))

(setup (:elpa org-download)
  (setq org-download-backend 't)
  (setq org-download-timestamp "%H%M%S")
  (setq org-download-screenshot-method "shotgun -g \"$(slop)\" %s")
  (setq-default org-download-image-dir "./images")
  (setq org-download-screenshot-basename "screenshot")
  (org-download-enable))

(defun org-babel-tangle-block()
  "Tangle only the current block."
  (interactive)
  (let ((current-prefix-arg '(4)))
    (call-interactively 'org-babel-tangle)))

(defun tangle-config ()
  "If the current buffer is in '' the code-blocks are tangled."
  (let ((file (buffer-file-name)))
    (when (and (string-match (expand-file-name m-org-dir) (buffer-file-name))
	       (eq major-mode 'org-mode))
      (async-start
       `(lambda ()
	  (require 'org)
	  (org-babel-tangle-file ,file))
       (lambda (result) (message "Tangled file."))))))
(add-hook 'after-save-hook 'tangle-config)

(setup (:elpa org-ref)
  (:also-elpa bibtex-completion)
  (require 'bibtex-completion)
  (require 'org-ref)
  (:option bibtex-completion-bibliography (list (expand-file-name "references/references.bib" uni-root-path))))

(setup (:elpa org-inline-pdf))

(setup (:elpa org-fragtog)
  (:hook-into org-mode-hook))

(add-to-list 'org-latex-logfiles-extensions "tex")
(setq org-preview-latex-default-process 'dvisvgm)
(setq org-preview-latex-image-directory ".ltximg/")
(setq org-latex-preview-ltxpng-directory ".ltximg/")
(setq org-format-latex-options (plist-put org-format-latex-options :scale 2.0))
(setq org-latex-compiler "xelatex")
(setq org-latex-remove-logfiles nil)
(setq org-latex-listings t)
(setq org-export-with-toc nil)
(setq org-export-coding-system 'utf-8-unix)
(setq org-latex-classes '(("article"
			   "\\documentclass{article}"
			   ("\\section{%s}" . "\\section*{%s}")
			   ("\\subsection{%s}" . "\\subsection*{%s}")
			   ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
			   ("\\paragraph{%s}" . "\\paragraph*{%s}")
			   ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))
			  ("book"
			   "\\documentclass{memoir}"
			   ("\\section{%s}" . "\\section*{%s}")
			   ("\\subsection{%s}" . "\\subsection*{%s}")
			   ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
			   ("\\paragraph{%s}" . "\\paragraph*{%s}")
			   ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))))

(setup (:elpa org-reveal)
  (require 'ox-reveal)
  (:option org-reveal-root (expand-file-name "revealjs" m-cache-dir)
	   org-reveal-plugins '(highlight)
	   org-reveal-theme "serif"
	   org-reveal-single-file t
	   org-reveal-transition "none"
	   org-reveal-revealjs-version "4")
  (defun org-reveal-export-to-html-and-browse
      (&optional async subtreep visible-only body-only ext-plist)
    "Export current buffer to a reveal.js and browse HTML file."
    (interactive)
    (browse-url (concat "file://" (expand-file-name (org-reveal-export-to-html async subtreep visible-only body-only ext-plist)) "?print-pdf"))))

(setup (:elpa literate-calc-mode))

(org-add-link-type
 "color"
 (lambda (path) "No follow action.")
 (lambda (color description backend)
   (cond ((eq backend 'latex)                  ; added by TL
	  (format "{\\color{%s}%s}" color description)) ; added by TL
	 ((eq backend 'html)
	  (let ((rgb (assoc color color-name-rgb-alist)) r g b)
	    (if rgb
		(progn
		  (setq r (* 255 (/ (nth 1 rgb) 65535.0))
			g (* 255 (/ (nth 2 rgb) 65535.0))
			b (* 255 (/ (nth 3 rgb) 65535.0)))
		  (format "<span style=\"color: rgb(%s,%s,%s)\">%s</span>"
			  (truncate r) (truncate g) (truncate b)
			  (or description color)))
	      (format "No Color RGB for %s" color)))))))

(defun get-word-regional ()
  "Get word at cursor or current region."
  (let* ((bds (bounds-of-thing-at-point 'symbol))
	 (beg (car bds))
	 (end (cdr bds)))
    (if (use-region-p)
	(yank (kill-region (region-beginning) (region-end)))
      (progn (mark-current-word)
	     (yank (kill-region (region-beginning) (region-end)))))))

(defun org-colorize ()
  "Add color to text at point."
  (interactive)
  (org-insert-link nil (concat "color:" (read-string "Color name: ")) (get-word-regional)))

(provide 'm-org)
