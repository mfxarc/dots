;;; m-elisp.el -*- lexical-binding: t; -*-

(setup (:elpa helpful)
  (setq helpful-max-buffers 1)
  (setq read-symbol-positions-list nil)
  (advice-add 'describe-function :override #'helpful-function)


  (advice-add 'describe-variable :override #'helpful-variable)
  (advice-add 'describe-command :override #'helpful-callable)
  (advice-add 'describe-key :override #'helpful-key)
  (advice-add 'describe-symbol :override #'helpful-symbol))

(setup (:elpa highlight-parentheses)
  (:hook-into smartparens-mode)
  (setq hl-paren-colors '("red1" "orange1" "cyan1" "green1"))
  (setq hl-paren-background-colors '(nil nil nil)))

(setup (:elpa smartparens)
  (:hook-into prog-mode text-mode)
  (setq sp-show-pair-from-inside t)
  (setq sp-cancel-autoskip-on-backward-movement nil))

(setup (:elpa eros)
  (setq eros-overlays-use-font-lock nil)
  (eros-mode))

(setup emacs-lisp-mode
  (:elpa aggressive-indent)
  (:hook 'aggressive-indent-mode))

(setup (:elpa macrostep))

(setup emacs-lisp-mode
  (:hook 'outline-minor-mode)
  (:hook 'hs-minor-mode))

(setup emacs-lisp-mode
  (global-eldoc-mode -1)
  (:hook eldoc-mode))

(provide 'm-elisp)
