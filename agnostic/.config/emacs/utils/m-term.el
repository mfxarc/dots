;;; m-term.el -*- lexical-binding: t; -*-

(setup (:elpa vterm-toggle))
(setup (:elpa vterm)
  (:also-elpa eterm-256color)
  (setq vterm-term-environment-variable "eterm-color")
  (setq vterm-kill-buffer-on-exit t)
  (setq vterm-buffer-name "vterm")
  (setq vterm-max-scrollback 5000)
  (setq vterm-timer-delay 0)
  (setq confirm-kill-process 'nil))

(defun make-bottom-window ()
  (progn (toggle-window-split)
	 (make-window-half)))
(add-hook 'vterm-toggle-show-hook 'make-bottom-window)

(provide 'm-term)
