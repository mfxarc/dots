;;; m-evil.el -*- lexical-binding: t; -*-

(setup (:elpa evil)
  (:option evil-want-C-u-scroll t
	   evil-want-keybinding nil)
  (evil-mode)
  (dolist (mode '(eshell-mode git-rebase-mode term-mode))
    (add-to-list 'evil-emacs-state-modes mode))
  (add-to-list 'evil-normal-state-modes 'messages-buffer-mode)
  (add-to-list 'evil-insert-state-modes 'vterm-mode))

(setup (:elpa general)
  (require 'general))

(define-transient-command helpful-management
  "Helpful transient management."
  [
   ["Helpful"
    ("s" "Describe symbol at point" xref-find-definitions-other-window)
    ("f" "Describe function" helpful-function)
    ("v" "Describe variable" helpful-variable)
    ]
   [
    ""
    ("k" "Describe key" helpful-key)
    ("c" "Describe face" describe-face)
    ]
   ["Straight"
    ("u p" "Install package" straight-use-package)
    ("u d" "Pull package" straight-pull-package-and-deps)
    ("u r" "Remove unused packages" straight-remove-unused-repos)
    ]
   ])

(define-transient-command notes-management ()
  "Notes transient management."
  [
   [
    ("f" "Find or create note" notes-find)
    ("c" "Create a note" note-create)
    ("t" "Capture todo" org-capture)
    ]
   ])

(define-transient-command toggle-management
  "Toggles transient management."
  [
   ["Toggle"
    ("w"  "Word wrap" toggle-word-wrap)
    ("t"  "Truncate lines" toggle-truncate-lines)
    ("l"  "Line numbers" display-line-numbers-mode)
    ("C-f" "Flyspell" flyspell-mode)
    ]
   ])

(define-transient-command uni-note-menu ()
  "Menu to manage university notes."
  [
   ["Creating"
    ("n"  "Note" uni-new-note)
    ("C-c"  "Class" uni-new-class)
    ]
   ["List"
    ("f"  "Notes" uni-find-note)
    ("v"  "PDF" uni-find-pdf)
    ("c"  "Classes" uni-find-class)
    ]
   [""
    ("l"  "Books" uni-find-book)
    ]
   ["Searching"
    ("s"  "Notes by class" uni-find-note-class)
    ("r"  "Notes by regex" uni-find-rg)
    ]
   ["Management"
    ("p"  "Open notes folder" uni-open-folder)
    ("d"  "Delete note" uni-delete-note)
    ("C-r" "Rename note" uni-rename-note)
    ]
   ["Insert"
    ("o"  "Note" uni-insert-note)
    ("i"  "Reference" org-ref-insert-cite-link)
    ]
   ["Export"
    ("e"  "PDF" uni-publish-pdf)
    ]])

(define-transient-command buffer-management
  "Buffer transient management."
  ["Buffer"
   ("b" "Scratch with current major mode" scratch)
   ("n" "New blank w/o mode" evil-buffer-new)
   ("r" "Revert chanes" revert-buffer)
   ("k" "Kill current buffer" kill-current-buffer)
   ("l" "List of buffers" ibuffer)
   ])

(define-transient-command editor-management
  "Editor transient management."
  [
   ["Case"
    ("u"  "Upper" string-inflection-upcase)
    ("l"  "Lower" string-inflection-lower-camelcase)
    ("t"  "Capitalize" string-inflection-toggle)
    ]
   ["Symbols"
    ("c"   "Comment line" evilnc-comment-or-uncomment-lines)
    ("p"   "Parentheses" insert-parentheses)
    ("b"   "Brackets" insert-brackets)
    ]
   [""
    ("w"   "Writer quotes" insert-writer-quotes)
    ("q"   "Code Quotes" insert-double-quotes)
    ("v"   "Code S-Quotes" insert-quotes)
    ]
   ["Lorem Ipsum"
    ("i p" "Paragraph" lorem-ipsum-insert-paragraphs)
    ("i s" "Sentence" lorem-ipsum-insert-sentences)
    ("i l" "List" lorem-ipsum-insert-list)
    ]
   ["Hour/Date"
    ("i h" "Hour" insert-hour)
    ("i d" "Date" insert-date)
    ("i c" "Calendar" prompt-date)
    ]
   ])

(define-transient-command search-management ()
  "Search transient management."
  [
   ["Errors"
    ("f" "Code with flymake" consult-flymake)
    ("s" "Spelling with flyspell" consult-flyspell)
    ]
   ])

(define-transient-command org-menu ()
  "Menu to manage org-mode."
  [
   ["Insert"
    ("l" "Link" org-insert-link)
    ("p" "Image" org-download-yank)
    ("d" "Table" org-table-create)
    ]
   [""
    ("f" "Format" format-buffer)
    ]
   ["Export"
    ("e" "Various formats" org-export-dispatch)
    ("b" "Babel block" org-babel-tangle-block)
    ("v" "Babel whole file" org-babel-tangle)
    ("c" "Babel clean results" org-babel-remove-result-one-or-many)
    ]
   ["Toggle"
    ("t" "Todo" org-todo)
    ("h" "Heading" org-toggle-heading)
    ("x" "Checkbox state"org-toggle-checkbox)
    ]
   [""
    ("i" "Bullet item"org-toggle-item)
    ("I" "Inline image" org-toggle-inline-images)
    ("s" "Sort" org-sort)
    ]
   ])

(define-transient-command linguist-management
  "Checker transient management."
  [
   ["Check"
    ("w" "Spell error at point" flyspell-correct-at-point)
    ("p" "Pronunciation at point" speak-at-point)
    ]
   ["Languages"
    ("t" "Translate word/region" translate-at-point)
    ("d" "Define word" define-word-at-point)
    ]
   ])

(define-transient-command file-management ()
  "Manage your files, archives and folders."
  [
   ["Find"
    ("g" "File in current dir" find-file)
    ("f" "File in work-dirs" fd-file)
    ("r" "Recent files" consult-buffer)
    ("j" "Current directory" dired-jump)
    ]
   ["Save"
    ("s" "Save" save-buffer)
    ("a" "Save as..." write-file)
    ]
   ["Create"
    ("c f" "File" make-empty-face)
    ("c d" "Folder" make-directory)
    ]
   ["Others"
    ("d f" "Delete file" delete-file)
    ("d d" "Delete directory" delete-directory)
    ("b" "Rename file" rename-file)
    ("v" "Copy file" copy-file)
    ]
   ["Sudo"
    ("u" "Find file"  sudo-find-file)
    ("e" "Edit current buffer" sudo-edit-file)
    ]
   ])

(define-transient-command hugo-management ()
  "Manage your hugo blog."
  [
   ["Manage posts"
    ("n" "Create" hugo-new-post)
    ("f" "Find" hugo-find-post)
    ("d" "Delete" hugo-delete-post)
    ("r" "Ripgrep" hugo-ripgrep)
    ]
   ["Manage repository"
    ("u" "Update public dir" hugo-update-blog)
    ("c" "Commit changes" hugo-commit-blog)
    ("l" "Push to repo" hugo-lazy-update)
    ("s" "Magit-status" hugo-magit)
    ]
   ["Open"
    ("o" "Blog in browser" hugo-open-blog)
    ("p" "Preview" hugo-open-preview)
    ]
   ["Private repo"
    ("C-l" "Push" hugo-private-update)
    ]
   ])

(general-define-key
 :states '(normal visual motion replace emacs insert)
 "s-u"   'switch-to-previous-buffer
 "s-ç"   'toggle-single-window
 "s-]"   'toggle-theme
 "s-C-ç" 'toggle-window-split
 "s-c"   'kill-window
 "C-."   'olivetti-expand
 "C-,"   'olivetti-shrink
 "C-p" 'consult-yank-pop
 "C-w" 'centaur-tabs-switch-group
 "M-e" 'centaur-tabs-forward-group
 "M-q" 'centaur-tabs-backward-group
 "C-e" 'centaur-tabs-forward
 "C-f" 'consult-outline
 "C-s" 'consult-line
 "M-j" 'move-line-down
 "M-k" 'move-line-up)
(general-define-key
 :states '(normal visual motion replace emacs)
 "C-q" 'centaur-tabs-backward)
(general-define-key
 :states '(normal visual motion replace)
 "RET" 'nil
 "g"   'nil
 "n"   'nil
 ":"   'eval-expression
 "C-a" 'crux-move-beginning-of-line
 "C-l" 'end-line
 "gr" 'rotate-text
 "gb" 'goto-last-change
 "gg" 'beginning-of-buffer
 "gc" 'avy-goto-char
 "gw" 'avy-goto-word-0
 "gl" 'avy-goto-line
 "gh" 'end-of-buffer
 "nf" '(format-buffer :which-key "Format buffer"))
(general-define-key
 :states 'insert
 "C-q" 'evil-normal-state
 "C-h" 'backward-delete-char)
(general-define-key
 :states 'insert
 "C-f" 'corfu-insert
 "C-j" 'corfu-next
 "C-k" 'corfu-previous
 "C-q" 'evil-normal-state
 "C-h" 'backward-delete-char)
(defun none () (interactive))

(general-define-key
 :keymaps '(normal visual emacs)
 :prefix "SPC"
 "SPC" 'execute-extended-command
 "f"  'file-management
 "b"  'buffer-management
 "h"  'helpful-management
 "o"  'notes-management
 "t"  'toggle-management
 "g"  'hugo-management
 "q"  'editor-management
 "c"  'linguist-management
 "u"  'uni-note-menu
 "s"  'search-management
 "a"  'consult-bookmark
 "ns" 'magit-status-dotfiles
 "nf" 'magit-status
 "TAB" 'cycle-ispell-languages
 "v"   'vterm-toggle
 "C-s" 'consult-ripgrep
 "ww" 'neotree-toggle
 "wc" 'evil-window-delete
 "wl" 'evil-window-right
 "wh" 'evil-window-left
 "wk" 'evil-window-up
 "wj" 'evil-window-down
 "wv" 'evil-window-vsplit
 "ws" 'evil-window-split)

(general-define-key
 :states '(normal visual emacs motion insert)
 :keymaps '(dict-mode-map helpful-mode-map dired-mode-map image-mode-map special-mode-map messages-buffer-mode-map elfeed-show-mode-map)
 "q" 'kill-window)

(general-define-key :states '(normal visual) :keymaps 'org-mode-map "n" '(org-menu :which-key "Org"))
(general-define-key
 :states '(normal visual)
 :keymaps 'org-mode-map
 "TAB" 'org-cycle
 "C-<return>" 'org-meta-return
 "C-l" 'org-metaright
 "C-h" 'org-metaleft)
(general-define-key
 :states 'normal
 :prefix "C-n"
 :keymaps 'org-mode-map
 :predicate '(org-table-p)
 "l" '(org-table-insert-hline :which-key "Insert line")
 "c" '(org-table-insert-column :which-key "Insert column")
 "r" '(org-table-insert-row :which-key "Insert row")
 "d" '(org-table-delete-column :which-key "Delete column")
 "f" '(org-table-eval-formula :which-key "Insert formula")
 "i" '(org-table-field-info :which-key "Field info")
 "s" '(org-table-sum :which-key "Sum column"))
(general-define-key
 :states 'normal
 :keymaps 'org-mode-map
 :predicate '(org-table-p)
 "H" '(org-table-move-cell-left :which-key "Move cell left")
 "L" '(org-table-move-cell-right :which-key "Move cell right")
 "K" '(org-table-move-cell-up :which-key "Move cell up")
 "J" '(org-table-move-cell-down :which-key "Move cell down"))
(general-define-key
 :states 'normal
 :keymaps 'org-src-mode-map
 "C-c C-c" 'org-edit-src-exit)

(general-define-key
 :keymaps 'org-agenda-mode-map
 "SPC" 'nil
 "g"   'nil
 "j"   'org-agenda-next-item
 "k"   'org-agenda-previous-item
 "l"   'org-agenda-later
 "h"   'org-agenda-earlier
 "v"   '(org-agenda-show :which-key "Show entry")
 "g"   '(:ignore t :which-key "Go to")
 "g d" '(org-agenda-goto-date :which-key "Date")
 "g h" '(org-agenda-goto-today :which-key "Today"))

(general-define-key
 :states '(normal visual motion emacs)
 :prefix "n"
 :keymaps 'emacs-lisp-mode-map
 ""    '(:ignore t :which-key "Elisp")
 "TAB" '(hs-toggle-hiding :which-key "Fold")
 "e"   '(macrostep-expand :which-key "Expand macro")
 "b"   '(eval-buffer :which-key "Eval buffer")
 "d"   '(eval-defun :which-key "Eval defun")
 "r"   '(eval-region :which-key "Eval region")
 "l"   '(eval-last-sexp :which-key "Eval last expression"))

(general-define-key
 :states '(normal visual motion emacs)
 :prefix "n"
 :keymaps 'LaTeX-mode-map
 "n" '(TeX-insert-macro :which-key "Insert macro")
 "h" '(latex-section :which-key "Toggle section"))
(general-define-key
 :states '(normal 'visual)
 :keymaps 'LaTeX-mode-map
 "TAB"   '(TeX-fold-dwim :which-key "Fold macro"))

(general-define-key
 :keymaps 'vertico-map
 "C-j" 'vertico-next
 "C-k" 'vertico-previous
 "C-f" 'vertico-exit)
(general-define-key
 :keymaps 'minibuffer-local-map
 "C-h" 'minibuffer-backward-kill)

(general-define-key
 :states 'normal
 :keymaps 'dired-mode-map
 "j" 'dired-next
 "k" 'dired-prev
 ;;-- NAVIGATION
 "u"   'evil-scroll-up
 "d"   'evil-scroll-down
 "h"   'dired-up-directory
 "l"   'dired-find-file
 "C-l" 'dired-find-file-other-window
 ;;-- MARKING
 "f"   'dired-mark
 "n"   'dired-unmark
 "t"   'dired-toggle-marks
 ;;-- INTERFACE
 "s"   'dired-hide-dotfiles-mode
 "C-h" 'dired-hide-details-mode
 "C-f" 'dired-sort
 ;;-- CREATE
 "c d" 'dired-create-directory
 "c f" 'dired-create-empty-file
 ;;-- COPY/MOVE
 "o"   'dired-do-hardlink
 "y"   'dired-ranger-copy
 "Y"   'dired-do-copy
 "C-y" 'dired-copy-path
 "x"   'dired-ranger-move
 ;;-- PASTE
 "p"   'dired-ranger-paste
 ;;-- DELETE
 "C-d" 'dired-do-delete
 ;;-- EXTERNAL
 "C-o" 'dired-fm
 "C-t" 'dired-term
 "C-c" 'dired-classifier
 "C-i" 'dired-share-kdeconnect
 "a"   'wdired-change-to-wdired-mode
 "i"   'wdired-change-to-wdired-mode
 "M"   'dired-chmod
 "O"   'dired-chown
 "ed"  'epa-dired-do-decrypt
 "ee"  'epa-dired-do-encrypt
 "z"   'dired-do-compress)

(general-define-key
 :states '(normal visual motion emacs)
 :keymaps 'ibuffer-mode-map
 "h"  'kill-current-buffer
 "f"  'ibuffer-mark-forward
 "d"  'ibuffer-do-delete
 "l"  'ibuffer-do-view
 "j"  'evil-next-line
 "k"  'evil-previous-line)

(general-define-key
 :states 'normal
 :keymaps 'neotree-mode-map
 "j" 'neotree-next
 "k" 'neotree-prev
 "l" 'neotree-open
 "TAB" 'neotree-cycle
 "C-l" 'neotree-open-file-in-system-application
 "C-h" 'neotree-hidden-file-toggle
 "C-y" 'neotree-copy-filepath-to-yank-ring
 "s" 'neotree-enter-horizontal-split
 "v" 'neotree-enter-vertical-split
 "y" 'neotree-copy-node
 "r" 'neotree-rename-node
 "d" 'neotree-delete-node
 "c" 'neotree-create-node
 "h" 'neotree-back)

(general-define-key
 :keymaps 'magit-status-mode-map
 "p"   'magit-push
 "C-d" 'evil-scroll-down
 "C-u" 'evil-scroll-up
 "v"   'evil-visual-line
 "j"   'magit-next-line
 "k"   'magit-previous-line
 "l"   'magit-section-toggle
 "h"   'magit-diff-visit-file
 "C-r" 'magit-refresh-all
 "C-l" 'magit-log-all)
(general-define-key
 :keymaps 'magit-log-mode-map
 "l" 'magit-show-commit
 "j"   'evil-next-line
 "k"   'evil-previous-line)

(general-define-key
 :keymaps 'vterm-mode-map
 :states 'insert
 "C-y" 'vterm-yank
 "C-v" 'vterm-toggle-hide
 "C-p" 'vterm-yank-pop
 "C-h" 'vterm-send-C-h
 "C-j" 'vterm-send-C-j)

(provide 'm-evil)
