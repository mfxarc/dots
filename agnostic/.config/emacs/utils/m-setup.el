;;; m-setup.el -*- lexical-binding: t; -*-
(straight-use-package 'setup)
(require 'setup)

(defmacro defsetup (name signature &rest body)
  "Shorthand for `setup-define'.
NAME is the name of the local macro.  SIGNATURE is used as the
argument list for FN.  If BODY starts with a string, use this as
the value for :documentation.  Any following keywords are passed
as OPTS to `setup-define'."
  (declare (debug defun))
  (let (opts)
    (when (stringp (car body))
      (setq opts (nconc (list :documentation (pop body)) opts)))
    (while (keywordp (car body))
      (let* ((prop (pop body)) (val `',(pop body)))
	(setq opts (nconc (list prop val) opts))))
    `(setup-define ,name (cl-function (lambda ,signature ,@body)) ,@opts)))

(defsetup :elpa (recipe &optional require)
	  "Install RECIPE with 'straight-use-package'. This macro can be used as HEAD,
and will replace itself with the first RECIPE's package."
	  :repeatable t
	  :shorthand (lambda (x)
		       (let ((recipe (cadr x)))
			 (if (consp recipe) (car recipe) recipe)))
	  `(straight-use-package ',recipe))

(defsetup :also-elpa (recipe)
	  "Install RECIPE with 'straight-use-package'. This macro can be used as HEAD,
and will replace itself with the first RECIPE's package."
	  :repeatable t
	  :after-loaded t
	  `(setup (:elpa ,recipe)))

(defsetup :github (recipe repo)
	  "Install GitHub RECIPE from REPO with straight-use-package."
	  :repeatable t
	  `(straight-use-package '(,recipe :host github :repo ,repo)))

(defsetup :load-after (features &rest body)
	  "Load the current feature after FEATURES."
	  :indent 1
	  (let ((body `(progn (require ',(setup-get 'feature)) ,@body)))
	    (dolist (feature (nreverse (ensure-list features)))
	      (setq body `(with-eval-after-load ',feature ,body))) body))

(provide 'm-setup)
