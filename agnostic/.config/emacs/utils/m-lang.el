;;; m-lang.el -*- lexical-binding: t; -*-

(setup (:elpa format-all))

(defun indent-buffer ()
  (interactive)
  (progn (indent-region (point-min) (point-max))
	 (whitespace-cleanup)
	 (save-buffer)))
(defun org-cleanup ()
  (interactive)
  (progn (org-edit-special)
	 (indent-buffer)
	 (org-edit-src-exit)))
(defun format-buffer ()
  (interactive)
  (progn (cond ((org-in-src-block-p) (org-cleanup))
	       ((eq major-mode 'org-mode) (indent-buffer))
	       (t (format-all-buffer)))
	 (save-buffer)))

(setup (:elpa auctex)
  (add-to-list 'auto-mode-alist '("\\.tex\\'" . LaTeX-mode))
  (:with-hook LaTeX-mode-hook
    (:hook 'TeX-fold-mode))
  (:with-hook after-save-hook
    (:hook 'latex-compile)))

(defun latex-compile ()
  "Compile latex document and open in Zathura."
  (interactive)
  (let* ((buffer (buffer-name))
	 (base (file-name-base buffer))
	 (ext (file-name-extension buffer))
	 (pdf (concat (file-name-base buffer) ".pdf")))
    (when (and (eq major-mode 'latex-mode)
	       (string-match "tex" ext))
      (progn (call-process-shell-command (format "xelatex -shell-escape -interaction=batchmode %s 1>/dev/null &" buffer))
	     (call-process-shell-command (format "biber %s 1>/dev/null &" base))
	     (call-process-shell-command (format "xelatex -shell-escape -interaction=batchmode %s 1>/dev/null &" buffer))
	     (when (not (split-string (shell-command-to-string (format "wmctrl -l | rg %s" pdf))))
	       (call-process-shell-command (format "zathura %s &" pdf)))
	     (message "Document %s compiled" base)))))

(defun TeX-insert-macro (symbol)
  (interactive (list (completing-read (concat "Macro: " TeX-esc)
				      (TeX-symbol-list-filtered) nil nil nil
				      'TeX-macro-history TeX-default-macro)))
  (when (called-interactively-p 'any)
    (setq TeX-default-macro symbol))
  (TeX-parse-macro symbol (cdr-safe (assoc symbol (TeX-symbol-list))))
  (evil-insert-state)
  (run-hooks 'TeX-after-insert-macro-hook))

(defun latex-section ()
"Toggle section"
(interactive)
(progn (beginning-of-line)
       (insert "\\section{")
       (end-of-line)
       (insert "}")))

(defun lilypond-compile ()
  "Compile lilypond document and open in Zathura."
  (interactive)
  (let* ((file (buffer-name))
	 (ext (file-name-extension file))
	 (pdf (concat (file-name-base file) ".pdf")))
    (when (string-match "ly" ext)
      (let ((command (format "lilypond -s %s" file)))
	(compilation-start command)
	(when (not (split-string (shell-command-to-string (format "wmctrl -l | rg %s" pdf))))
	  (call-process-shell-command (format "zathura %s &" pdf)))))))

(defun groff-compile ()
  "Compile groff document and open in Zathura."
  (interactive)
  (let* ((buffer (buffer-name))
	 (ext (file-name-extension buffer))
	 (pdf (concat (file-name-base buffer) ".pdf")))
    (when (and (eq major-mode 'nroff-mode)
	       (string-match "ms" ext))
      (progn (call-process-shell-command (format "groff -k -ms -tbl -T pdf %s > %s" buffer pdf))
	     (when (not (split-string (shell-command-to-string (format "wmctrl -l | rg %s" pdf))))
	       (call-process-shell-command (format "zathura %s &" pdf)))
	     (message "Document %s compiled" buffer)))))
(add-hook 'after-save-hook 'groff-compile)

(setup (:elpa haskell-mode)
  (:file-match "\\.hs\\'"))

(setup (:elpa flymake-lua))
(setup (:elpa lua-mode)
  (:file-match "\\.lua\\'")
  (:hook 'flymake-lua-load)
  (setq lua-indent-level 2))

(setup (:elpa json-mode)
  (:file-match "\\.json\\'")
  (:hook 'json-mode-beautify))

(setup (:elpa csv-mode)
  (:file-match "\\.csv\\'")
  (:when-loaded (add-hook 'csv-mode-hook 'csv-align-mode)))

(setup css-mode
  (:file-match "\\.css\\'" "\\.rasi\\'"))

(setup (:elpa clojure-mode)
  (:file-match "\\.clj\\'"))

(push '("ui\\'" . xml-mode) auto-mode-alist)
(push '("conky.conf" . lua-mode) auto-mode-alist)
(push '("list\\'" . conf-mode) auto-mode-alist)
(push '("rc\\'" . conf-mode) auto-mode-alist)
(push '("xresources" . conf-mode) auto-mode-alist)

(setup (:elpa flymake-shellcheck)
  (add-hook 'sh-mode-hook 'flymake-shellcheck-load)
  (add-hook 'sh-mode-hook 'flymake-mode))

(defun shell-save-hook ()
  "Ensure that shell script files use correct major-mode."
  (when (save-excursion (goto-char (point-min)) (looking-at "^#!/")) (shell-script-mode)))
(add-hook 'after-save-hook 'shell-save-hook)
(add-hook 'after-save-hook 'executable-make-buffer-file-executable-if-script-p)

(provide 'm-lang)
