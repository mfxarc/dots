;;; init.el -*- lexical-binding: t; -*-

(require 'user)

(require 'm-setup)

(setup (:elpa pos-tip)
  (require 'pos-tip))
(setup (:elpa transient)
  (require 'transient)
  (:option transient-show-common-commands nil))
(setup (:elpa popup)
  (require 'popup))
(setup (:github async "jwiegley/emacs-async"))
(setup (:elpa alert)
  (:option alert-default-style 'notifications))
(require 'tramp)

(require 'm-evil)
(require 'm-ui)
(require 'm-elisp)
(require 'm-edit)
(require 'm-org)
(require 'm-lang)
(require 'm-git)
(require 'm-term)
(require 'm-dir)

(require 'e-note)
(require 'e-uni)
(require 'e-blog)
(require 'e-ling)

(server-start)
