import subprocess
import os
from qutebrowser.api import interceptor

config.load_autoconfig(False)
c.auto_save.session = True
c.window.title_format = '{perc}{current_url}'
c.qt.force_platformtheme = 'qt5ct'

c.content.headers.accept_language = 'en-US,en;q=0.5'
c.content.blocking.method = 'both'
c.content.blocking.adblock.lists = ["https://easylist.to/easylist/easylist.txt", "https://easylist-downloads.adblockplus.org/easylistportuguese.txt", "https://easylist-downloads.adblockplus.org/easyprivacy.txt", "https://raw.githubusercontent.com/hoshsadiq/adblock-nocoin-list/master/nocoin.txt", "https://raw.githubusercontent.com/Spam404/lists/master/adblock-list.txt"]
c.content.cookies.accept = 'all'
c.content.headers.user_agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.74 Safari/537.36'

c.content.pdfjs = False
c.downloads.location.prompt = False
c.downloads.remove_finished = 10
c.scrolling.smooth = True
c.content.prefers_reduced_motion = True
c.content.media.audio_video_capture = True
c.content.media.audio_capture = True

c.fonts.default_size = '14pt'
c.zoom.default = '115%'
c.tabs.tabs_are_windows = True
c.tabs.show = 'never'
c.statusbar.show = 'never'
c.colors.webpage.darkmode.enabled = False
c.colors.webpage.darkmode.policy.images = 'never'
c.content.prefers_reduced_motion = True
c.downloads.position = 'bottom'
c.statusbar.widgets = ["url"]
c.statusbar.padding = {'bottom': 1,'left': 2,'right': 15,'top':1}

c.url.default_page = 'file:///media/personal/notes/journal/notes/links.html'
c.url.start_pages = 'file:///media/personal/notes/journal/notes/links.html'
c.url.searchengines = {
    'DEFAULT': 'https://search.brave.com/search?q={}',
    'am': 'https://www.amazon.com/s?k={}',
    'aw': 'https://wiki.archlinux.org/?search={}',
    'go': 'https://www.google.com/search?q={}',
    'rd': 'https://www.reddit.com/r/{}',
    'di': 'https://www.urbandictionary.com/define.php?term={}',
    'wiki': 'https://en.wikipedia.org/wiki/{}',
    'ytt': 'https://www.youtube.com/results?search_query={}',
    'be': 'https://www.behance.net/?tracking_source=typeahead_search_direct&search={}',
    'br': 'https://search.brave.com/search?q={}',
    'yt': 'https://yewtu.be/search?q={}',
    'ml': 'https://lista.mercadolivre.com.br/{}',
    'lib': 'https://br1lib.org/s/{}',
    'wiki': 'https://en.wikipedia.org/wiki/{}',
    'sch': 'https://scholar.google.com/scholar?q={}',
    'arch': 'https://wiki.archlinux.org/index.php?search={}',
    'cta': 'https://www.ctan.org/search?phrase={}',
    'zoom': 'https://www.zoom.com.br/search?q={}',
    'mvs': 'https://yts.pm/browse-movies/{}',
    'gh': 'https://github.com/search?q={}',
    'img': 'https://unsplash.com/s/photos/{}',
    'gt': 'https://www.classclef.com/?s={}',
    'dr': 'https://dribbble.com/search/{}',
    'fi': 'https://www.flaticon.com/search?word={}',
    'da': 'https://www.deviantart.com/search?q={}',
    'am': 'https://www.amazon.com.br/s?k={}',
    'nt': 'https://sflix.cc/?s={}',
    'go': 'https://www.google.com/search?q={}',
    'pt': 'https://br.pinterest.com/search/pins/?q={}',
    'fl': 'https://flathub.org/apps/search/{}',
    'al': 'https://alternativeto.net/browse/search/?q={}',
    'rd': 'https://www.reddit.com/search/?q={}',
    'isbn': 'https://www.worldcat.org/search?qt=worldcat_org_all&q={}',
    'pix': 'https://pixabay.com/images/search/{}',
    'libgen': 'http://libgen.rs/search.php?req={}',
    'pirate': 'https://thepiratebay.org/search.php?q={}',
    'gutenberg': 'https://www.gutenberg.org/ebooks/search/?query={}'
}
c.downloads.location.directory = '~/dl'

c.bindings.default = {}
config.bind('e', 'set-cmd-text :')
config.bind('r', 'reload')
config.bind('<Ctrl-w>', 'tab-close')
config.bind('<Ctrl-q>', 'quit')
config.bind('<Ctrl-r>', 'config-source')
config.bind('<Ctrl-p>', 'print')

config.bind('d', 'scroll-page 0 0.5')
config.bind('u', 'scroll-page 0 -0.5')
config.bind('<Ctrl-k>', 'zoom-in')
config.bind('<Ctrl-j>', 'zoom-out')
config.bind('h', 'scroll left')
config.bind('j', 'scroll-px 0 80')
config.bind('k', 'scroll-px 0 -80')
config.bind('l', 'scroll right')
config.bind('<Ctrl-h>', 'back')
config.bind('<Ctrl-l>', 'forward')
config.bind('gg', 'scroll-to-perc 0')
config.bind('gh', 'scroll-to-perc')

config.bind('s', 'set-cmd-text /')
config.bind('n', 'search-next')
config.bind('<Ctrl-n>', 'search-prev')

config.bind('v', 'mode-enter caret')
config.bind('y', 'yank selection', mode='caret')
config.bind('v', 'selection-toggle', mode='caret')
config.bind('<Ctrl-V>', 'selection-toggle --line', mode='caret')
config.bind('h', 'move-to-prev-char', mode='caret')
config.bind('j', 'move-to-next-line', mode='caret')
config.bind('k', 'move-to-prev-line', mode='caret')
config.bind('l', 'move-to-next-char', mode='caret')
config.bind('w', 'move-to-next-word', mode='caret')
config.bind('b', 'move-to-prev-word', mode='caret')

config.bind('t', 'open -t')
config.bind('o', 'set-cmd-text -s :open')
config.bind('<Ctrl-o>', 'set-cmd-text -s :open -w')

config.bind('f', 'hint')
config.bind('<Ctrl-f>', 'hint all tab')
config.bind('yy', 'yank inline [[{url}][{title}]]')
config.bind('yl', 'yank')
config.bind('yf', 'hint links yank')
config.bind('yd', 'hint links download')
config.bind('ym', 'hint links spawn --detach mpv {hint-url}')
config.bind('m', 'spawn --detach mpv {url}')

config.bind('xs', 'view-source')
config.bind('xb', 'config-cycle statusbar.show always never')
config.bind('xu', 'config-cycle colors.webpage.darkmode.enabled True False')
config.bind('xt', 'config-cycle tabs.show always never')
config.bind('xh', 'config-cycle content.user_stylesheets ~/.config/qutebrowser/css/gruvbox.css ""')
config.bind('xl', 'config-cycle content.user_stylesheets ~/.config/qutebrowser/css/solarized.css ""')

config.bind('i', 'mode-enter insert')
config.bind("<Ctrl-l>", "fake-key <Return>", "insert")
config.bind("<Ctrl-w>", "fake-key <Home>", "insert")
config.bind("<Ctrl-e>", "fake-key <End>", "insert")
config.bind("<Ctrl-a>", "fake-key <Ctrl-a>", "insert")

config.bind("<Ctrl-b>", "fake-key <Left>", "insert")
config.bind("<Ctrl-f>", "fake-key <Right>", "insert")
config.bind("<Ctrl-p>", "fake-key <Up>", "insert")
config.bind("<Ctrl-n>", "fake-key <Down>", "insert")

config.bind("<Ctrl-h>", "fake-key <Backspace>", "insert")
config.bind("<Ctrl-u>", "fake-key <Shift-Home><Delete>", "insert")
config.bind("<Ctrl-k>", "fake-key <Shift-End><Delete>", "insert")

config.bind('<Ctrl-d>', 'mode-leave', mode='insert')
config.bind('<Ctrl-d>', 'mode-leave', mode='hint')
config.bind('<Ctrl-d>', 'mode-leave', mode='prompt')
config.bind('<Ctrl-d>', 'prompt-accept no', mode='yesno')
config.bind('<Ctrl-d>', 'mode-leave', mode='command')
config.bind('<Ctrl-d>', 'mode-leave', mode='caret')
config.bind('<Return>', 'prompt-accept yes', mode='yesno')
config.bind('<Ctrl-l>', 'prompt-accept --save yes', mode='yesno')
config.bind('<Ctrl-l>', 'hint-accept', mode='hint')
config.bind('<Ctrl-l>', 'prompt-accept --save yes', mode='prompt')
config.bind('<Ctrl-l>', 'command-accept', mode='command')
config.bind('<Ctrl-h>', 'rl-backward-delete-char', mode='prompt')
config.bind('<Ctrl-h>', 'rl-backward-delete-char', mode='command')
config.bind('<Ctrl-j>', 'prompt-item-focus next', mode='prompt')
config.bind('<Ctrl-k>', 'prompt-item-focus prev', mode='prompt')
config.bind('<Ctrl-j>', 'completion-item-focus next', mode='command')
config.bind('<Ctrl-k>', 'completion-item-focus prev', mode='command')

bg0_hard = "#1d2021"
bg0_soft = '#32302f'
bg0_normal = '#282828'
bg0 = bg0_normal
bg1 = "#3c3836"
bg2 = "#504945"
bg3 = "#665c54"
bg4 = "#7c6f64"
fg0 = "#d5c4a1"
fg1 = "#d5c4a1"
fg2 = "#d5c4a1"
fg3 = "#bdae93"
fg4 = "#a89984"
bright_red = "#fb4934"
bright_green = "#b8bb26"
bright_yellow = "#fabd2f"
bright_blue = "#83a598"
bright_purple = "#d3869b"
bright_aqua = "#8ec07c"
bright_gray = "#928374"
bright_orange = "#fe8019"
dark_red = "#cc241d"
dark_green = "#98971a"
dark_yellow = "#d79921"
dark_blue = "#458588"
dark_purple = "#b16286"
dark_aqua = "#689d6a"
dark_gray = "#a89984"
dark_orange = "#d65d0e"
c.colors.completion.fg = [fg1, bright_aqua, bright_yellow]
c.colors.completion.odd.bg = bg0
c.colors.completion.even.bg = c.colors.completion.odd.bg
c.colors.completion.category.fg = bright_blue
c.colors.completion.category.bg = bg1
c.colors.completion.category.border.top = c.colors.completion.category.bg
c.colors.completion.category.border.bottom = c.colors.completion.category.bg
c.colors.completion.item.selected.fg = fg0
c.colors.completion.item.selected.bg = bg4
c.colors.completion.item.selected.border.top = bg2
c.colors.completion.item.selected.border.bottom = c.colors.completion.item.selected.border.top
c.colors.completion.item.selected.match.fg = bright_orange
c.colors.completion.match.fg = c.colors.completion.item.selected.match.fg
c.colors.completion.scrollbar.fg = c.colors.completion.item.selected.fg
c.colors.completion.scrollbar.bg = c.colors.completion.category.bg
c.colors.contextmenu.disabled.bg = bg3
c.colors.contextmenu.disabled.fg = fg3
c.colors.contextmenu.menu.bg = bg0
c.colors.contextmenu.menu.fg = fg2
c.colors.contextmenu.selected.bg = bg2
c.colors.contextmenu.selected.fg = c.colors.contextmenu.menu.fg
c.colors.downloads.bar.bg = bg0
c.colors.downloads.start.fg = bg0
c.colors.downloads.start.bg = bright_blue
c.colors.downloads.stop.fg = c.colors.downloads.start.fg
c.colors.downloads.stop.bg = bright_aqua
c.colors.downloads.error.fg = bright_red
c.colors.hints.fg = bg0
c.colors.hints.bg = 'rgba(250, 191, 47, 200)'  # bright_yellow
c.colors.hints.match.fg = bg4
c.colors.keyhint.fg = fg4
c.colors.keyhint.suffix.fg = fg0
c.colors.keyhint.bg = bg0
c.colors.messages.error.fg = bg0
c.colors.messages.error.bg = bright_red
c.colors.messages.error.border = c.colors.messages.error.bg
c.colors.messages.warning.fg = bg0
c.colors.messages.warning.bg = bright_purple
c.colors.messages.warning.border = c.colors.messages.warning.bg
c.colors.messages.info.fg = fg2
c.colors.messages.info.bg = bg0
c.colors.messages.info.border = c.colors.messages.info.bg
c.colors.prompts.fg = fg2
c.colors.prompts.border = f'1px solid {bg1}'
c.colors.prompts.bg = bg3
c.colors.prompts.selected.bg = bg2
c.colors.statusbar.normal.fg = fg2
c.colors.statusbar.normal.bg = bg0
c.colors.statusbar.insert.fg = bg0
c.colors.statusbar.insert.bg = dark_aqua
c.colors.statusbar.passthrough.fg = bg0
c.colors.statusbar.passthrough.bg = dark_blue
c.colors.statusbar.private.fg = bright_purple
c.colors.statusbar.private.bg = bg0
c.colors.statusbar.command.fg = fg3
c.colors.statusbar.command.bg = bg1
c.colors.statusbar.command.private.fg = c.colors.statusbar.private.fg
c.colors.statusbar.command.private.bg = c.colors.statusbar.command.bg
c.colors.statusbar.caret.fg = bg0
c.colors.statusbar.caret.bg = dark_purple
c.colors.statusbar.caret.selection.fg = c.colors.statusbar.caret.fg
c.colors.statusbar.caret.selection.bg = bright_purple
c.colors.statusbar.progress.bg = bright_blue
c.colors.statusbar.url.fg = fg4
c.colors.statusbar.url.error.fg = dark_red
c.colors.statusbar.url.hover.fg = bright_orange
c.colors.statusbar.url.success.http.fg = bright_red
c.colors.statusbar.url.success.https.fg = fg0
c.colors.statusbar.url.warn.fg = bright_purple
c.colors.tabs.bar.bg = bg0
c.colors.tabs.indicator.start = bright_blue
c.colors.tabs.indicator.stop = bright_aqua
c.colors.tabs.indicator.error = bright_red
c.colors.tabs.odd.fg = fg2
c.colors.tabs.odd.bg = bg2
c.colors.tabs.even.fg = c.colors.tabs.odd.fg
c.colors.tabs.even.bg = bg3
c.colors.tabs.selected.odd.fg = fg2
c.colors.tabs.selected.odd.bg = bg0
c.colors.tabs.selected.even.fg = c.colors.tabs.selected.odd.fg
c.colors.tabs.selected.even.bg = bg0
c.colors.tabs.pinned.even.bg = bright_green
c.colors.tabs.pinned.even.fg = bg2
c.colors.tabs.pinned.odd.bg = bright_green
c.colors.tabs.pinned.odd.fg = c.colors.tabs.pinned.even.fg
c.colors.tabs.pinned.selected.even.bg = bg0
c.colors.tabs.pinned.selected.even.fg = c.colors.tabs.selected.odd.fg
c.colors.tabs.pinned.selected.odd.bg = c.colors.tabs.pinned.selected.even.bg
c.colors.tabs.pinned.selected.odd.fg = c.colors.tabs.selected.odd.fg
