import Control.Monad (liftM2, when)
import qualified Data.Map as M
import Data.Maybe (fromJust, isJust)
import Data.Monoid
import Data.Ratio
import System.Environment (getArgs)
import System.IO (hPutStrLn)
import XMonad

import XMonad.Actions.CopyWindow (kill1)
import XMonad.Actions.CycleWS (nextWS, prevWS, shiftToNext, shiftToPrev, toggleWS)
import XMonad.Actions.FloatKeys
import XMonad.Actions.Minimize (maximizeWindowAndFocus, minimizeWindow, withLastMinimized)
import XMonad.Actions.MouseResize (mouseResize)
import XMonad.Actions.WithAll (killAll)

import XMonad.Hooks.EwmhDesktops (ewmh, ewmhFullscreen)
import XMonad.Hooks.ManageDocks (ToggleStruts (..), avoidStruts, docks, manageDocks)
import XMonad.Hooks.ManageHelpers (doCenterFloat, doFullFloat, doRectFloat, doSink, isFullscreen)
import XMonad.Hooks.Minimize
import XMonad.Hooks.ScreenCorners
import XMonad.Hooks.WindowSwallowing (swallowEventHook)

import XMonad.Layout.BorderResize
import qualified XMonad.Layout.BoringWindows as BW
import XMonad.Layout.LimitWindows (limitWindows)
import XMonad.Layout.Minimize (minimize)
import XMonad.Layout.Monitor
import XMonad.Layout.MultiToggle (EOT (EOT), mkToggle, single, (??))
import qualified XMonad.Layout.MultiToggle as MT (Toggle (..))
import XMonad.Layout.MultiToggle.Instances (StdTransformers (MIRROR, NBFULL, NOBORDERS))
import XMonad.Layout.NoBorders
import XMonad.Layout.ResizableTile
import XMonad.Layout.SimplestFloat (simplestFloat)
import XMonad.Layout.Tabbed
import XMonad.Layout.SubLayouts
import XMonad.Layout.Simplest
import qualified XMonad.Layout.ToggleLayouts as T (ToggleLayout (Toggle), toggleLayouts)
import XMonad.Layout.WindowArranger (WindowArrangerMsg (..), windowArrange)

import qualified XMonad.StackSet as S (StackSet, greedyView, shift)
import qualified XMonad.StackSet as W
import XMonad.Util.EZConfig (additionalKeysP)
import XMonad.Util.Replace (replace)
import XMonad.Util.Run (spawnPipe)
import XMonad.Util.SpawnOnce (spawnOnce)
windowCount :: X (Maybe String)
windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset

myStartupHook = do
  spawnOnce "hsetroot -cover ~/pix/poetica/europeana.jpg &"
  spawnOnce "xsetroot -cursor_name left_ptr &"
  spawnOnce "picom -b &"
  spawnOnce "dunst &"
  spawnOnce "dbus-update-activation-environment --systemd DBUS_SESSION_BUS_ADDRESS DISPLAY XAUTHORITY &"
  spawnOnce "gnome-keyring-daemon -s &"
  spawnOnce "/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 >/dev/null 2>&1 &"
  spawnOnce "syncthing --no-browser &"
  spawnOnce "/usr/lib/kdeconnectd &"
  spawnOnce "fcitx5 &"
  spawnOnce "ulauncher --no-window-shadow &"
  spawnOnce "emacs"
  spawnOnce "xrdb ~/.config/xresources/poetica && st -e topgrade"
  spawnOnce "conky -d"
  spawnOnce "nm-applet"
  spawnOnce "xset r rate 170 40"
  addScreenCorners
    [ (SCUpperRight, nextWS),
      (SCUpperLeft, prevWS)
    ]

myModMask = mod4Mask -- Sets modkey to super/windows key

myTerminal = "st" -- Default terminal emulator
myEditor = "emacsclient -qcn -a '/usr/local/bin/emacs'" -- Primary text editor
myNDEditor = myTerminal ++ " -T nvim -e nvim " -- Secondary text editor
myBrowser = "browser" -- Default browser
myFileManager = "emacsclient -cne '(dired-jump)'" -- File manager

myFont = "xft:Sarasa UI K:weight=bold:pixelsize=12:antialias=true:hinting=true;"
myBorderWidth = 2 -- Border width of windows's border
myNormColor = "#1F1F1F" -- Border color of normal windows
myFocusColor = "#BBAC8D" -- Border color of focused windows
myTabTheme =
  def
    { fontName = myFont,
      activeColor = "#C9C1B5",
      activeBorderColor = "#C9C1B5",
      activeTextColor = "#282828",
      inactiveColor = "#282828",
      inactiveBorderColor = "#282828",
      inactiveTextColor = "#928374"
    }

tall = addTabs shrinkText myTabTheme $ subLayout [] (Simplest) $ withBorder myBorderWidth $ limitWindows 3 (ResizableTall 1 (3 / 100) (1 / 2) [])
tabs = withBorder 0 $ limitWindows 7 (tabbedBottomAlways shrinkText myTabTheme)
panel = Mirror $ tall
floats = simplestFloat

myLayoutHook =
  smartBorders $
    borderResize $
      screenCornerLayoutHook $
	minimize $
	  BW.boringWindows $
	    avoidStruts $
	      mouseResize $
		windowArrange $
		  T.toggleLayouts floats $
		    mkToggle (NBFULL ?? NOBORDERS ?? EOT) myDefaultLayout
  where
    myDefaultLayout = tabs ||| tall ||| panel

myWorkspaces = [" I ", " II ", " III ", " IV ", " VI ", " VII ", " VIII "]

myWorkspaceIndices = M.fromList $ zipWith (,) myWorkspaces [1 ..]

myHandleEventHook = swallowEventHook (className =? "St") (return True) <+> minimizeEventHook <+> screenCornerEventHook

devWS = ["Emacs", "nvim", "notion-app-enhanced", "Zathura"]
netWS = ["Min", "firefox-nightly", "librewolf", "qutebrowser", "Hamsket"]
sysWS = ["St", "Spacefm", "Timeshift-gtk"]
docWS = ["wpsoffice", "libreoffice", "libreoffice-writer", "MuseScore3", "Anki", "wps", "calibre", "Zotero"]
mediaWS = ["mpv", "Ario", "Lollypop", "Io.elementary.music"]
gfxWS = ["Gimp", "Gimp-2.10", "Gimp-2.99", "Akira", "Krita", "Darktable"]

floatW = ["GtkFileChooserDialog", "Dialog", "gimp-file-export", "gimp-file-open"]
floatCW = ["kdeconnect-urlhandler", "Picket"]
floatTW = ["Server autodetection", "Quick Format Citation", "Zotero - Document Preferences", "Zotero Metadata Importer"]

ignoreW = ["Popup"]
ignoreCW = ["Dunst"]

getProp a w = withDisplay $ \dpy -> io $ getWindowProperty32 dpy a w
checkDialog :: Query Bool
checkDialog =
  ask >>= \w -> liftX $ do
    a <- getAtom "_NET_WM_WINDOW_TYPE"
    dialog <- getAtom "_NET_WM_WINDOW_TYPE_DIALOG"
    mbr <- getProp a w
    case mbr of
      Just [r] -> return $ elem (fromIntegral r) [dialog]
      _ -> return False

myManageHook =
  composeAll . concat $
    -- Workspaces
    [ [className =? c --> viewShift (myWorkspaces !! 0) | c <- devWS],
      [className =? c --> viewShift (myWorkspaces !! 1) | c <- netWS],
      [className =? c --> viewShift (myWorkspaces !! 2) | c <- sysWS],
      [className =? c --> viewShift (myWorkspaces !! 3) | c <- docWS],
      [className =? c --> viewShift (myWorkspaces !! 4) | c <- mediaWS],
      [className =? c --> viewShift (myWorkspaces !! 5) | c <- gfxWS],
      [className =? "Ld-linux-x86-64.so.2" --> viewShift (myWorkspaces !! 6)],
      [className =? "Inkscape" --> viewShift (myWorkspaces !! 6)],
      [className =? "Xephyr" --> doSink],
      -- Float
      [title =? c --> rectFloat | c <- floatTW],
      [className =? c --> rectFloat | c <- floatCW],
      [role =? c --> rectFloat | c <- floatW],
      [checkDialog --> rectFloat],
      -- Ignore
      [role =? c --> doIgnore | c <- ignoreW],
      [className =? c --> doIgnore | c <- ignoreCW],
      -- Fullscreen
      [isFullscreen --> (doF W.focusDown <+> doFullFloat)]
    ]
  where
    viewShift = doF . liftM2 (.) S.greedyView S.shift
    role = stringProperty "WM_WINDOW_ROLE"
    rectFloat = doRectFloat (W.RationalRect (1 / 6) (1 / 6) (1 / 1.5) (1 / 1.5))

myKeys =
  [ ("M-C-r", spawn "xmonad --recompile && xmonad --restart"), -- Recompiles and restart xmonad
    ("M-S-r", spawn "xmonad --restart"), -- Restart xmonad
    -- Apps
    ("M-e", spawn (myTerminal)),
    ("M-r", spawn (myFileManager)),
    ("M-w", spawn "word-lookup"),
    ("M-0", spawn (myEditor)),
    ("M-9", spawn "emacsclient --eval '(emacs-everywhere)'"),
    ("M-8", spawn (myNDEditor)),
    ("M-<Return>", spawn (myBrowser)),
    --- Emacs
    ("M-<Down>", spawn "emacsclient -e '(org-timer-pause-or-continue)'"),
    ("M-<Right>", spawn "emacsclient -e '(org-timer-start)'"),
    ("M-<Left>", spawn "emacsclient -e '(org-timer-stop)'"),
    --- Menu scripts
    ("M-q", spawn "power"),
    ("M-a", spawn "bmarks"),
    ("M-y", spawn "addbmark"),
    ("M-p", spawn "shellcmd"),
    ("M-s", spawn "search"),
    ("<F4>", spawn "colorclip"),
    ("<F3>", spawn "picket"),
    -- Workspace management
    ("M-v", toggleWS), -- Switch to last used workspace
    ("M-z", prevWS), -- Switch to previous workspace
    ("M-x", nextWS), -- Switch to next workspace
    ("M-o", nextWS), -- Switch to next workspace
    ("M-i", prevWS), -- Switch to previous workspace
    ("M-C-i", shiftToPrev >> prevWS), -- Move window and switch to next workspace
    ("M-C-o", shiftToNext >> nextWS), -- Move window and switch to previous workspace
    -- Window management
    ("M-C-c", kill1), -- Kill the currently focused client
    ("M-C-f", withFocused toggleFloat), -- Toggles windows to float
    ("M-m", withFocused minimizeWindow),
    ("M-C-m", withLastMinimized maximizeWindowAndFocus),
    ---- Resize window in master/stack
    ("M-h", sendMessage Shrink), -- Shrink width
    ("M-l", sendMessage Expand), -- Expand width
    ("M-M1-l", sendMessage MirrorShrink), -- Expand height
    ("M-M1-h", sendMessage MirrorExpand), -- Shrink height
    -------- Resize window to float
    ("M-C-h", withFocused (keysResizeWindow (-40, 0) (0, 0))), -- Shrink width
    ("M-C-l", withFocused (keysResizeWindow (40, 0) (0, 0))), -- Expand width
    ("M-C-j", withFocused (keysResizeWindow (0, 40) (0, 0))), -- Expand height
    ("M-C-k", withFocused (keysResizeWindow (0, -40) (0, 0))), -- Shrink height
    ("M-C-S-h", withFocused (keysMoveWindow (-30, 0))), -- Move window left
    ("M-C-S-l", withFocused (keysMoveWindow (30, 0))), -- Move window right
    ("M-C-S-j", withFocused (keysMoveWindow (0, 30))), -- Move window down
    ("M-C-S-k", withFocused (keysMoveWindow (0, -30))), -- Move window up
    -- Windows navigation
    ("M-j", BW.focusDown), -- Move focus to the next window
    ("M-k", BW.focusUp), -- Move focus to the prev window
    ("M-S-j", BW.swapDown), -- Swap focused window with next window
    ("M-S-k", BW.swapUp), -- Swap focused window with prev window
    -- Layouts
    ("M-<Space>", sendMessage NextLayout), -- Switch to next layout
    ("M-f", sendMessage (MT.Toggle NBFULL) >> sendMessage (ToggleStruts)), -- Toggles fullscreen
    -- SubLayouts
    ("M-<Tab>", onGroup W.focusUp'),
    ("M-S-h", sendMessage $ pullGroup L),
    ("M-S-l", sendMessage $ pullGroup R),
    ("M-C-a", withFocused (sendMessage . MergeAll)),
    ("M-C-s", withFocused (sendMessage . UnMerge)),
    -- Multimedia Keys
    ("<Print>", spawn "dmmaim"),
    ("S-<Print>", spawn "sleep 2 && notify-send 'Screenshot taken' && shotgun"),
    ("<Pause>", spawn "pactl -- set-sink-volume 0 -2%"),
    ("<Scroll_lock>", spawn "pactl -- set-sink-volume 0 +2%")
  ]
  where
    toggleFloat w =
      windows
	( \s ->
	    if M.member w (W.floating s)
	      then W.sink w s
	      else (W.float w (W.RationalRect 0.25 0.25 0.5 0.5) s))

main = do
  replace
  args <- getArgs
  when ("--replace" `elem` args) replace
  xmproc <- spawnPipe "killall polybar ; polybar"
  xmonad $
    ewmhFullscreen $
      ewmh $
	docks $
	  def
	    { manageHook = myManageHook <+> manageDocks,
	      handleEventHook = myHandleEventHook,
	      modMask = myModMask,
	      terminal = myTerminal,
	      startupHook = myStartupHook,
	      layoutHook = myLayoutHook,
	      workspaces = myWorkspaces,
	      normalBorderColor = myNormColor,
	      focusedBorderColor = myFocusColor
	    }
	    `additionalKeysP` myKeys
